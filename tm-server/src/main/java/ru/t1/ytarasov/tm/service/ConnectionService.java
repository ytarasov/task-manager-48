package ru.t1.ytarasov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Session;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }
    @Override
    public @NotNull EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl());
        settings.put(Environment.USER, propertyService.getDBUsername());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getShowSql());
        /*settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCash());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getConfigFilePath());*/
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return factory().createEntityManager();
    }

}
